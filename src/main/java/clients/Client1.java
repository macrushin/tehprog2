package clients;

import converter.FIleConverter;
import entities.File;

import java.io.*;
import java.net.Socket;
import java.util.stream.Stream;

public class Client1 {
    private static Socket clientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 8081);
                while(true) {
                    reader = new BufferedReader(new InputStreamReader(System.in));
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                    System.out.println("Введетите полный адрес файла");
                    String fileLocation = reader.readLine();
                    if ("exit".equals(fileLocation)) {
                        break;
                    }
                    if (!fileLocation.isEmpty()) {
                        InputStream inStream = new FileInputStream(fileLocation);
                        byte[] bytes = inStream.readAllBytes();
                        entities.File file = new entities.File();
                        file.setBytes(bytes);
                        file.setFormat(getFormat(fileLocation));
                        String json = FIleConverter.toJSON(file);
                        out.write(json + "\n");
                        out.flush();
                        inStream.close();
                    }
                    String serverJson = in.readLine();
                    File fileFromServer = FIleConverter.toJavaObject(serverJson);
                    String fileName = Integer.toString((int) (Math.random() * (100001)));

                    OutputStream outStream = new FileOutputStream("C:\\stud\\TehProg\\attachments\\" +
                            fileName + "." +
                            fileFromServer.getFormat());
                    outStream.write(fileFromServer.getBytes());
                    outStream.close();
                }
            } finally {
                System.out.println("Клиент был закрыт...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    private static String getFormat(String fileLocation) {
        return fileLocation.substring(fileLocation.lastIndexOf(".") + 1);
    }
}
