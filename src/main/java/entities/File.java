package entities;

import com.fasterxml.jackson.annotation.JsonProperty;


public class File {

    @JsonProperty("format")
    private String format;

    @JsonProperty("bytes")
    private byte[] bytes;


    public File() {
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
