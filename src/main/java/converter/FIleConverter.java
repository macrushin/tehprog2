package converter;

import entities.File;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class FIleConverter {
    public static String toJSON(File file) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(file);
    }

    public static File toJavaObject(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, File.class);
    }
}
