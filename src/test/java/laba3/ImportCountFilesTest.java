package laba3;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ImportCountFilesTest {
    @Test
    public void validation() throws ValidationException, FileNotFoundException {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(new FileInputStream("schema.json")));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(new FileInputStream("countFiles.json")));

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }
}
